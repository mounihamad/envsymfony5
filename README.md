# Symfony 5 + PHP 7.4 + phpmyadmin + mysql avec Docker

Déloyer un environnement symfony sur un code existant

# Prérequis

Avoir docker et docker-compose installer sur la machine hote

# Procédure

Apres avoir cloner ce projet

```
  git clone https://gitlab.com/mounihamad/envsymfony5.git
```

se deplacer dans le dossier du projet

```
  cd EnvSymfony5
```

Modifier la ligne du docker-compose suivante et remplacer le nom db-data par le chemin
absolue d'un dossier en local pour avoir une trace de la bd:

```
  volumes:
    - db-data:/var/lib/mysql
```

Modifier la ligne du docker-compose suivante et remplacer apiSiren par le chemin du dossier de l'api:

```
  volumes:
    - ./apiSiren:/var/www/html
```

Modifier aussi les lignes du docker-compose suivantes et mettre le nom d'utilisateur, le
mot de passe et le nom de la base de BD se trouvant dans le dossier .env de l'api:

```
  MYSQL_DATABASE: siren
  MYSQL_USER: nirou
  MYSQL_PASSWORD: nirouPassword
```

Tapper la commande suivante dans la console pour executer le docker-compose

```bash
  docker build -t api-siren .
  docker-compose up -d
```

Pour entrer dans le container PHP, executer la commande suivante

```bash
  docker exec -it api bash
```

Une fois à l'intérieur du conteneur et du dossier contenant le code, vérifiez le contenu. Si le code est présent, vous pouvez éventuellement installer les dépendances avec la commande suivante :

```bash
  composer install
```

Go to the code foder and update the .env file, in the DATABASE_URL line, set the variable.
For our top configuration we have:

```bash
DATABASE_URL="mysql://nirou:nirouPassword@mysql-db:3306/siren"
```

## Api

_L'interface de l'api est disponible sur http://localhost:5000 ou http://127.0.0.1:5000_

## BD

_L'interface phpmyadmin est disponible sur http://localhost:8080 ou http://127.0.0.1:8080_
