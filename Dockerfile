FROM php:7.4-apache

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf 

# Installer les dépendances nécessaires
RUN apt-get update && apt-get install -y \
    libicu-dev \
    git \
    unzip \
    zip

# Activer les extensions PHP nécessaires pour Symfony
RUN docker-php-ext-install intl pdo_mysql

WORKDIR /var/www/html
RUN curl -sS https://getcomposer.org/installer | php -- &&  mv composer.phar /usr/local/bin/composer 
RUN curl -sS https://get.symfony.com/cli/installer | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/

# Configurer Apache pour exécuter Symfony
RUN a2enmod rewrite
COPY apache.conf /etc/apache2/sites-available/000-default.conf

RUN adduser nirou
USER nirou
EXPOSE 80
CMD ["apache2-foreground"]